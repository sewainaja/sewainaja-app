import {NgModule} from '@angular/core';
import {ModuleWithProviders} from '@angular/compiler/src/core';
import {Api} from './libraries/api';
import {PaymentService} from './api/payment.service';
import {LeaseService} from './api/lease.service';
import {ProductService} from './api/product.service';
import {AuthService} from './api/auth.service';
import {UserService} from './api/user.service';

@NgModule()
export class ServiceModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: ServiceModule,
            providers: [
                Api,
                PaymentService,
                LeaseService,
                ProductService,
                AuthService,
                UserService
            ]
        };
    }
}
