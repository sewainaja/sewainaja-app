// the API for user address

import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()
export class UserService {

    constructor(private api: Api) { }

    //return user address to the database

    getUserAddresses(): Observable<any> {
        return this.api.get(`user/address`);
    }


    submitUserAddress(body: any): Observable<any> {
        return this.api.post(`user/address`, body);
    }
}
