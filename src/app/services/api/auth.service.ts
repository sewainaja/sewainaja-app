// this the API that we use for authentication

import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';

@Injectable()
export class AuthService {

    constructor(private api: Api) { }

    // API for loggin in returning JWT token

    login(body) {
        return this.api.postJson('auth/login', body);
    }

    // API for registering a new account returning data back to the database

    register(body) {
        return this.api.postJson('auth/register', body);
    }

    //for returning own account data to be displayed

    me() {
        return this.api.get('auth/me');
    }

    //API for loggin out

    logout() {
        return this.api.get('auth/logout');
    }
}
