// the API for leasing

import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()
export class LeaseService {

    constructor(private api: Api) {}

    //API for getting transaction data by ID of user

    public getById(transactionId: number): Observable<any> {
        return this.api.get(`lease/transactions/${transactionId}`);
    }

    // API for returning the data of item in check out
    public createLeaseTransaction(body: any): Observable<any> {
        return this.api.postJson(`lease/checkout`, body);
    }

    //API for returning delivery data

    public leaseDelivery(body: any): Observable<any> {
        return this.api.postJson(`lease/delivery`, body);
    }

    //API for returning the status of transaction

    public getPaidTransactionProvider(): Observable<any> {
        return this.api.get('lease/transactions-status', {
            test: 'haha',
            status: 'paid(1)'
        });
    }
}
