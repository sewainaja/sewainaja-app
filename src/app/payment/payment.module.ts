import {NgModule} from '@angular/core';
import {CheckoutComponent} from './checkout/checkout.component';
import {NgxStripeModule} from 'ngx-stripe';
import {CommonModule} from '@angular/common';
import {PaymentRoutingModule} from './payment.routes';
import {PaymentTemplateComponent} from './payment.template';
import {ReactiveFormsModule} from '@angular/forms';
import {ServiceModule} from '../services/service.module';
import {RouterModule} from '@angular/router';

const COMPONENTS: any[] = [
    CheckoutComponent,
    PaymentTemplateComponent
];

@NgModule({
    imports: [
        CommonModule,
        NgxStripeModule,
        PaymentRoutingModule,
        ReactiveFormsModule,
        RouterModule,
        ServiceModule
    ],
    declarations: [...COMPONENTS],
    exports: [...COMPONENTS],
    providers: []
})
export class PaymentModule {}
