import {Component, OnInit} from '@angular/core';
import {tap} from 'rxjs/operators';
import {mapToData} from '../../services/transformers/response.transformer';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../services/api/product.service';
import {HttpClient} from '@angular/common/http';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    public products: any;
    public NewProducts: any;
    public news: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private productService: ProductService,
        private http: HttpClient
    ) { }

    ngOnInit() {
        this.productService.getHomeProduct().pipe(
            mapToData(),
            tap((product: any) => {
                this.products = product;
            }),
        ).subscribe();

        this.productService.getHomeNewProduct().pipe(
            mapToData(),
            tap((product: any) => {
                this.NewProducts = product;
            }),
        ).subscribe();

        this.http.get('https://news.sewainaja.me/wp-json/wp/v2/posts').subscribe(res => {
            this.news = res;
        });
    }

}
