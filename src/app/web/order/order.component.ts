import {Component, OnInit} from '@angular/core';
import {LeaseService} from '../../services/api/lease.service';
import {tap} from 'rxjs/operators';
import {mapToData} from '../../services/transformers/response.transformer';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

    public transactions: any;

    constructor(private leaseService: LeaseService) {
    }

    ngOnInit() {
        this.leaseService.getPaidTransactionProvider().pipe(
            mapToData(),
            tap((res: any) => {
                this.transactions = res;
            })
        ).subscribe();
    }

}
