import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {switchMap, tap} from 'rxjs/operators';
import {mapToData} from '../../services/transformers/response.transformer';
import {ProductService} from '../../services/api/product.service';
import {AuthService} from '../../services/api/auth.service';
import {UserService} from '../../services/api/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LeaseService} from '../../services/api/lease.service';
import {Router} from '@angular/router';


@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.components.scss']
})
export class CartComponent implements OnInit {

    public product: any;
    public addresses: any[];
    public choosingAddress: boolean;
    public selectedAddress: any;
    public addressForm: FormGroup;
    public userId: number;

    @ViewChild('chooseAddress') ChooseAddress: ElementRef;

    constructor(
        private productService: ProductService,
        private authService: AuthService,
        private userService: UserService,
        private fb: FormBuilder,
        private leaseService: LeaseService,
        private router: Router
    ) {
        this.choosingAddress = true;
    }

    ngOnInit() {
        this.productService.getProductDetail(localStorage.selectedProduct).pipe(
            mapToData(),
            tap(selectedProduct => {
                this.product = selectedProduct;
            }),
        ).subscribe();

        this.authService.me().pipe(
            mapToData(),
            switchMap((me: any) => {
                return this.getAddresses();

            }),
        ).subscribe();

        this.addressForm = this.fb.group({
            province: ['', Validators.required],
            city: ['', Validators.required],
            address: ['', Validators.required],
            zipcode: ['', Validators.required],
        });
    }

    getAddresses() {
        return this.userService.getUserAddresses().pipe(
            mapToData(),
            tap((addresses: any) => {
                this.addresses = addresses;
                if (this.addresses.length > 0) {
                    this.selectedAddress = this.addresses[0];
                    this.choosingAddress = true;
                }
            }),
        );
    }

    submitAddress() {
        this.userService.submitUserAddress(this.addressForm.getRawValue()).pipe(
            switchMap(result => {
                this.choosingAddress = true;
                return this.getAddresses();
            }),
        ).subscribe();
    }

    chooseNewAddress(address) {
        this.selectedAddress = address;
        this.closepopUp();
    }

    popUp() {
        this.ChooseAddress.nativeElement.classList.toggle('Visible');
    }

    closepopUp() {
        this.ChooseAddress.nativeElement.classList.toggle('Visible');
        if (this.addresses.length > 0) {
            this.choosingAddress = true;
        }
    }

    checkout() {
        const body = {
            product_id: this.product.id,
            quantity: 1,
            address_id: this.selectedAddress.id,
            product_price_id: this.product.product_prices[0].id,
            lease_time_recurrence: 3,
            start_date: '2019-04-03'
        };

        this.leaseService.createLeaseTransaction(body).pipe(
            mapToData(),
            tap((result: any) => {
                localStorage.removeItem('selectedProduct');

                const params = {
                    order_id: result.id
                };

                this.router.navigate(['/checkout'], {queryParams: params});
            }),
        ).subscribe();
    }

}
