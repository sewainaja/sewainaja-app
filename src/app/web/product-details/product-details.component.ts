import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../services/api/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap, tap} from 'rxjs/operators';
import {mapToData} from '../../services/transformers/response.transformer';

@Component({
        selector: 'app-product-details',
        templateUrl: './product-details.component.html',
        styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

        public product: any;

        constructor(
                private activatedRoute: ActivatedRoute,
                private productService: ProductService,
                private router: Router) {
        }

        addToCart() {
                localStorage.setItem('selectedProduct', this.product.id);
                this.router.navigate(['/cart']);
        }

        ngOnInit() {
                this.activatedRoute.params.pipe(
                        switchMap(params => {
                                return this.productService.getProductDetail(params.id).pipe(
                                        mapToData(),
                                        tap(productDetails => {
                                                this.product = productDetails;
                                        })
                                );
                        })
                ).subscribe();

        }

}
