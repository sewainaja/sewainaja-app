import {NgModule} from '@angular/core';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {CommonModule} from '@angular/common';
import {AuthTemplateComponent} from './auth.template';
import {AuthRoutingModule} from './auth.routes';
import {ReactiveFormsModule} from '@angular/forms';

const COMPONENTS: any = [
    AuthTemplateComponent,
    RegisterComponent,
    LoginComponent
];

@NgModule({
    imports: [
        CommonModule,
        AuthRoutingModule,
        ReactiveFormsModule
    ],
    exports: [...COMPONENTS],
    declarations: [...COMPONENTS]
})
export class AuthModule {}

