import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../../services/api/auth.service';
import {HttpErrorResponse} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(public auth: AuthService, public router: Router) {}

    canActivate() {
        return this.auth.me().pipe(
            map((user: any) => null !== user),
            catchError((e: HttpErrorResponse) => {
                this.router.navigate(['/auth/login']);

                return of(false);
            }),
        );
    }
}
