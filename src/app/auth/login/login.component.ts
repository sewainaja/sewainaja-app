import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/api/auth.service';
import {tap} from 'rxjs/operators';
import {mapToData} from '../../services/transformers/response.transformer';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private authService: AuthService,
        private router: Router,
    ) {}

    login() {
        this.authService.login(this.loginForm.getRawValue())
            .pipe(
                mapToData(),
                tap(result => {
                    localStorage.setItem('token', result.token);

                    this.router.navigate(['/']);
                }),
            ).subscribe();
    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
}
